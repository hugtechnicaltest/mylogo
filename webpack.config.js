const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    entry: {
        HugStyle: path.resolve(__dirname, './Source/HugStyle.jsx'),
        HugTemplates: path.resolve(__dirname, './Source/HugTemplate.jsx'),
    },

    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'Resource/Dist')
    },
    module: {
        rules: [{
                test: /\.(scss)$/,
                use: [{
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        // Interprets `@import` and `url()` like `import/require()` and will resolve them
                        loader: 'css-loader',
                        options: { minimize: true }
                    },
                    {
                        // Loader for webpack to process CSS with PostCSS
                        loader: 'postcss-loader',
                        options: {
                            plugins: function() {
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {
                        // Loads a SASS/SCSS file and compiles it to CSS
                        loader: 'sass-loader'
                    }
                ]
            },
            // The file loader loads the file and provides a relative URL path to the background
            // emitFile: false - Will result in the file not getting generated again
            {
                test: /\.(png|svg|jpg|gif|eot|ttf|woff|woff2)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]', // This ensures the url of the file uses the path provided in the URL
                        emitFile: false, // Does not generate the file again
                        outputPath: '../../' // This path is relative to the generated CSS file. Using this the path becomes the same as our Images folder
                    }
                }]
            }
        ]
    },
    watch:true,
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            path: path.resolve(__dirname, 'Resource/Style')
        }),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp:/\.(css)$/g,
            cssProcessor: require('cssnano'),
            cssProcessorPluginOptions: {
                preset: ['default', { discardComments: { removeAll: true } }],
            },
            canPrint: true
        })
    ],

};